//Étienne Plante - 1934877

package LinearAlgebra;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class Vector3dTest {

    @Test
    void getX()
    {
        Vector3d v = new Vector3d(3, 5, 1);
        assertEquals(3, v.getX());
    }

    @Test
    void getY()
    {
        Vector3d v = new Vector3d(3, 5, 1);
        assertEquals(5, v.getY());
    }

    @Test
    void getZ()
    {
        Vector3d v = new Vector3d(3, 5, 1);
        assertEquals(1, v.getZ());
    }

    @Test
    void magnitudeZero()
    {
        Vector3d zeroVector = new Vector3d(0, 0, 0);
        assertEquals(0, zeroVector.magnitude());
    }

    @Test
    void magnitudeNegative()
    {
        //Negative
        Vector3d negativeVector = new Vector3d(-4, -2, -6);
        assertEquals(7.483314774, negativeVector.magnitude(), 0.00001);
    }

    @Test
    void dotProductZero()
    {
        Vector3d zeroVector = new Vector3d(0, 0, 0);
        Vector3d baseVector = new Vector3d(2, 4, 1);

        assertEquals(0, baseVector.dotProduct(zeroVector));
    }

    @Test
    void dotProductNegative()
    {
        Vector3d negativeVector = new Vector3d(-4, -2, -6);
        Vector3d baseVector = new Vector3d(2, 4, 1);

        assertEquals(-22, baseVector.dotProduct(negativeVector), 0.00001);
    }

    @Test
    void add()
    {
        Vector3d baseVector = new Vector3d(2, 4, 1);
        Vector3d addVector = new Vector3d(3, 7, 2);
        Vector3d expectedVector = new Vector3d(5, 11, 3);

        assertEquals(baseVector.add(addVector).getX(), expectedVector.getX());
        assertEquals(baseVector.add(addVector).getY(), expectedVector.getY());
        assertEquals(baseVector.add(addVector).getZ(), expectedVector.getZ());
    }
}