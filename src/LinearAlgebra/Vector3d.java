//Étienne Plante - 1934877

package LinearAlgebra;

public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX()
    {
        return x;
    }

    public double getY()
    {
        return y;
    }

    public double getZ()
    {
        return z;
    }

    public double magnitude()
    {
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
    }

    public double dotProduct(Vector3d v)
    {
        return (x * v.x) + (y * v.y) + (z * v.z);
    }

    public Vector3d add(Vector3d v)
    {
        return new Vector3d(x + v.x, y + v.y, z + v.z);
    }
}
